package com.mitocode.service.impl;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.inject.Named;

import org.mindrot.jbcrypt.BCrypt;

import com.mitocode.dao.IUsuarioDAO;
import com.mitocode.model.Usuario;
import com.mitocode.service.IUsuarioService;

@Named
public class UsuarioServiceImpl implements IUsuarioService, Serializable {

	@EJB
	private IUsuarioDAO dao;

	@Override
	public Usuario login(Usuario us) {
		String clave = us.getContrasena();
		String claveHash = dao.traerPassHashed(us.getUsuario());

		try {
			if (BCrypt.checkpw(clave, claveHash)) {
				return dao.leerPorNombreUsuario(us.getUsuario());
			}
		} catch (Exception e) {
			throw e;
		}

		return new Usuario();
	}

	@Override
	public List<Usuario> listarPorUsuario(String usuario) {
		return dao.listarPorUsuario(usuario);
	}

	@Override
	public Integer verificarContrasena(Usuario us, String contrasenaAVerificar) {
		String contrasenaHash = us.getContrasena();
		
		try {
			if (BCrypt.checkpw(contrasenaAVerificar, contrasenaHash)) {
				return 1;
			}
		} catch (Exception e) {
			
		}
		return 0;
	}

	@Override
	public Integer registrar(Usuario t) throws Exception {
		return dao.registrar(t);
	}

	@Override
	public Integer modificar(Usuario t) throws Exception {
		return dao.modificar(t);
	}

	@Override
	public Integer eliminar(Usuario t) throws Exception {
		return dao.eliminar(t);
	}

	@Override
	public List<Usuario> listar() throws Exception {
		return dao.listar();
	}

	@Override
	public Usuario listarPorId(Usuario t) throws Exception {
		return dao.listarPorId(t);
	}

}
