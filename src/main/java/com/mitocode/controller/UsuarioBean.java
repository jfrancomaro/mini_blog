package com.mitocode.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.mindrot.jbcrypt.BCrypt;

import com.mitocode.model.Usuario;
import com.mitocode.service.IUsuarioService;

@Named
@ViewScoped
public class UsuarioBean implements Serializable {

	@Inject
	private IUsuarioService usuarioService;

	private Usuario usuario;
	private List<Usuario> listaUsuario;

	private String usuarioBuscado;
	private String nuevaContrasena;
	private String contrasenaAVerificar;

	@PostConstruct
	public void init() {
		this.usuario = new Usuario();
		this.listaUsuario = new ArrayList<>();
	}

	public void mostrarData(Usuario u) {
		this.usuario = u;
	}

	public void verificar() {
		
		try {
			if (usuarioService.verificarContrasena(usuario, contrasenaAVerificar) == 1) {
				this.usuario.setContraseñaVerificada(true);
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Aviso", "Cuenta Verificada"));
			} else {
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Aviso", "Pruebe nuevamente"));
			}		

		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Aviso", "Ha ocurrido un error"));
		}
	}

	public void actualizarUsuario() {

		try {
			if (nuevaContrasena != null && !nuevaContrasena.equalsIgnoreCase("")) {
				String nuevaClaveHash = BCrypt.hashpw(nuevaContrasena, BCrypt.gensalt());
				this.usuario.setContrasena(nuevaClaveHash);
				
				usuarioService.modificar(this.usuario);
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Aviso", "Usuario Actualizado"));
				this.usuario.setContraseñaVerificada(false);
			} else {
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Aviso", "Ingrese una nueva contraseña"));
			}
			
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Aviso", "Hubo un error al actualizar el usuario"));
		}
	}
	
	public void cancelarActualizar() {
		this.usuario.setContraseñaVerificada(false);
	}
	
	public void buscar() {

		try {
			this.listaUsuario = usuarioService.listarPorUsuario(usuarioBuscado);
		} catch (Exception e) {
		}
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public List<Usuario> getListaUsuario() {
		return listaUsuario;
	}

	public void setListaUsuario(List<Usuario> listaUsuario) {
		this.listaUsuario = listaUsuario;
	}

	public String getNuevaContrasena() {
		return nuevaContrasena;
	}

	public void setNuevaContrasena(String nuevaContrasena) {
		this.nuevaContrasena = nuevaContrasena;
	}

	public String getContrasenaAVerificar() {
		return contrasenaAVerificar;
	}

	public void setContrasenaAVerificar(String contrasenaAVerificar) {
		this.contrasenaAVerificar = contrasenaAVerificar;
	}

	public String getUsuarioBuscado() {
		return usuarioBuscado;
	}

	public void setUsuarioBuscado(String usuarioBuscado) {
		this.usuarioBuscado = usuarioBuscado;
	}

}
